### Repérez dans le fichier /etc/sudoers la ligne qui donne tous les droits au groupe wheel

``` bash
[fred@node1 ~]$ sudo cat /etc/sudoers | grep wheel
## Allows people in group wheel to run all commands
%wheel  ALL=(ALL)       ALL
# %wheel        ALL=(ALL)       NOPASSWD: ALL
```

### Créez un nouvel utilisateur

```bash
[fred@node1 ~]$ sudo useradd UserTest -m -g wheel -p Formation2021

[UserTest@node1 ~]$ cat /etc/passwd
[…]
UserTest:x:1001:10::/home/UserTest:/bin/bash

[UserTest@node1 ~]$ groups
Wheel

[UserTest@node1 home]$ ls
fred  UserTest
```

### Afficher le statut du service SSH
```bash
[fred@node1 ~]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2021-12-13 13:50:22 CET; 19min ago
```

### Déterminer sur quel port le service SSH écoute

```bash
[fred@node1 ~]$ sudo ss -luptn
Netid   State    Recv-Q   Send-Q       Local Address:Port       Peer Address:Port   Process
tcp     LISTEN   0        128                0.0.0.0:22              0.0.0.0:*       users:(("sshd",pid=763,fd=5))
tcp     LISTEN   0        128                   [::]:22                 [::]:*       users:(("sshd",pid=763,fd=7))

sudo ss -lutpn =
-l, --listening
              Display only listening sockets (these are omitted by default).
-u, --udp
              Display UDP sockets.
-t, --tcp
              Display TCP sockets.
-p, --processes
              Show process using socket.
-n, --numeric
              Do not try to resolve service names. Show exact bandwidth values, instead of human-readable.
              
```

### Déterminer l'utilisateur qui a lancé le processus SSH

```bash
[fred@node1 ~]$ ps -ef
fred        1733    1597  0 14:23 pts/0    00:00:00 ps -ef
```

### Changez le port d'écoute du service SSH

```bash
[fred@node1 ssh]$ sudo vi sshd_config

Mis sur 1100

[fred@node1 ssh]$ sudo ss -lutpn
Netid   State    Recv-Q   Send-Q      Local Address:Port       Peer Address:Port   Process
tcp     LISTEN   0        128               0.0.0.0:1100            0.0.0.0:*       users:(("sshd",pid=1761,fd=5))
tcp     LISTEN   0        128                  [::]:1100

### Firewall

[fred@node1 ssh]$ sudo firewall-cmd --permanent --add-port=1100/tcp
Success

[fred@node1 ssh]$ sudo firewall-cmd --reload
Success
```



🌞 Vérification

vérifiez le changement avec une commande ss

connectez vous depuis votre machine en SSH vers la VM, en précisant le nouveau port à utiliser


### Installez le paquet NGINX

```bash
[fred@node1 ssh]$ systemctl start nginx
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'nginx.service'.
Authenticating as: Fred (fred)
Password:
==== AUTHENTICATION COMPLETE ====
```

### Gestion de firewall

```bash
[fred@node1 ssh]$ sudo ss -lutpn
Netid             State               Recv-Q              Send-Q                           Local Address:Port                           Peer Address:Port             Process
tcp               LISTEN              0                   128                                    0.0.0.0:1100                                0.0.0.0:*                 users:(("sshd",pid=1761,fd=5))
tcp               LISTEN              0                   128                                    0.0.0.0:80                                  0.0.0.0:*                 users:(("nginx",pid=26082,fd=8),("nginx",pid=26081,fd=8))
tcp               LISTEN              0                   128                                       [::]:1100                                   [::]:*                 users:(("sshd",pid=1761,fd=7))
tcp               LISTEN              0                   128                                       [::]:80                                     [::]:*                 users:(("nginx",pid=26082,fd=9),("nginx",pid=26081,fd=9))

[fred@node1 ssh]$ sudo firewall-cmd --permanent --add-port=80/tcp
Success

[fred@node1 ssh]$ sudo firewall-cmd --list-all
[…]
  ports: 1100/tcp 80/tcp
  
```


### Vérifier que le service est accessible


```bash
C:\Users\Fred>curl 192.168.56.2
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
[…]
```

###Création d'un service
```bash
[fred@node1 system]$ sudo systemctl start web.service
[fred@node1 system]$ sudo systemctl status web.service
● web.service - Lancement de service
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-12-13 15:57:19 CET; 2s ago
 Main PID: 26882 (python3)
    Tasks: 1 (limit: 4944)
   Memory: 9.4M
   CGroup: /system.slice/web.service
           └─26882 /usr/bin/python3 -m http.server 8888
           
```
```bash

[fred@node1 system]$ sudo systemctl enable web.service
Created symlink /etc/systemd/system/multi-user.target.wants/web.service → /etc/systemd/system/web.service.
```

###Affiner la configuration du service
```bash
[fred@node1 srv]$ sudo chown web web
[fred@node1 srv]$ ls -al
total 0
drwxr-xr-x.  3 root root  17 Dec 13 16:15 .
dr-xr-xr-x. 17 root root 224 Dec 13 10:34 ..
drwxr-xr-x.  2 web  root   6 Dec 13 16:15 web
```
###Install MariaDB
```bash

[fred@db ~]$ sudo dnf install mariadb-server -y
[…]
Complete!
```

###Le service MariaDB
```bash
[fred@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.

[fred@db ~]$ systemctl start mariadb

[fred@db ~]$ systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-14 15:04:22 CET; 3s ago

[fred@db ~]$ sudo ss -lutpn
Netid   State    Recv-Q   Send-Q      Local Address:Port       Peer Address:Port   Process
tcp     LISTEN   0        128               0.0.0.0:22              0.0.0.0:*       users:(("sshd",pid=765,fd=5))
tcp     LISTEN   0        128                  [::]:22                 [::]:*       users:(("sshd",pid=765,fd=7))
tcp     LISTEN   0        80                      *:3306                  *:*       users:(("mysqld",pid=8676,fd=21)


[fred@db ~]$ ps -ef | grep sql
mysql       8676       1  0 15:04 ?        00:00:00 /usr/libexec/mysqld --basedir=/usr
fred        8754    5497  0 15:07 pts/0    00:00:00 grep --color=auto sql

```
###Firewall
```bash
[fred@db ~]$ sudo firewall-cmd --permanent --add-port=3306/tcp
success
[fred@db ~]$ sudo firewall-cmd --reload
success
[fred@db ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 3306/tcp
```

###Configuration élémentaire de la base
```bash
[fred@db ~]$ sudo mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)
Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] Y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] Y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] n
 ... skipping.

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] Y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] Y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```
###Préparation de la base en vue de l'utilisation par NextCloud
```bash
[fred@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 17
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.2.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.2.1.11';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```

###Installez sur la machine web.tp2.cesi la commande mysql
```bash
[fred@web ~]$ dnf provides mysql
Rocky Linux 8 - AppStream                                                           3.6 MB/s | 8.4 MB     00:02
Rocky Linux 8 - BaseOS                                                              1.5 MB/s | 3.6 MB     00:02
Rocky Linux 8 - Extras                                                               14 kB/s |  10 kB     00:00
Last metadata expiration check: 0:00:01 ago on Tue 14 Dec 2021 03:24:34 PM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7

[fred@web ~]$ sudo dnf install mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
```

###Tester la connexion
```bash
[fred@web ~]$ mysql -h 10.2.1.12 -P 3306 -u nextcloud -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 20
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> SHOW TABLES;
ERROR 1046 (3D000): No database selected
mysql>
```

###Installer Apache sur la machine web.tp2.cesi
```bash
[fred@web ~]$ install dnf httpd
[fred@web system]$ sudo systemctl start httpd

[fred@web system]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-14 16:24:44 CET; 10s ago

[fred@web system]$ sudo ss -lnutp
Netid       State        Recv-Q       Send-Q               Local Address:Port               Peer Address:Port       Process
tcp         LISTEN       0            128                        0.0.0.0:22                      0.0.0.0:*           users:(("sshd",pid=763,fd=5))
tcp         LISTEN       0            128                              *:80                            *:*           users:(("httpd",pid=6421,fd=4),("httpd",pid=6420,fd=4),("httpd",pid=6419,fd=4),("httpd",pid=6417,fd=4))
tcp         LISTEN       0            128                           [::]:22                         [::]:*           users:(("sshd",pid=763,fd=7))



[fred@web ~]$ sudo systemctl enable httpd
```

###Installer PHP
```bash
[fred@web ~]$ sudo dnf install epel-release
[sudo] password for fred:
Last metadata expiration check: 2:11:21 ago on Tue 14 Dec 2021 04:18:48 PM CET.
Dependencies resolved.
====================================================================================================================
 Package                        Architecture             Version                     Repository                Size
====================================================================================================================
Installing:
 epel-release                   noarch                   8-13.el8                    extras                    23 k

Transaction Summary
====================================================================================================================
Install  1 Package

Total download size: 23 k
Installed size: 35 k
Is this ok [y/N]: y
Downloading Packages:
epel-release-8-13.el8.noarch.rpm                                                    201 kB/s |  23 kB     00:00
--------------------------------------------------------------------------------------------------------------------
Total                                                                                63 kB/s |  23 kB     00:00
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                            1/1
  Installing       : epel-release-8-13.el8.noarch                                                               1/1
  Running scriptlet: epel-release-8-13.el8.noarch                                                               1/1
  Verifying        : epel-release-8-13.el8.noarch                                                               1/1

Installed:
  epel-release-8-13.el8.noarch

Complete!
```

###Analyser la conf Apache

```bash
[fred@web ~]$ ls /etc/httpd/conf.d
autoindex.conf  php74-php.conf  README  userdir.conf  welcome.conf
```

###Créer un VirtualHost qui accueillera NextCloud
```bash
[fred@web conf.d]$ cat virthost.conf
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp2.cesi

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>

[fred@web conf.d]$ ls
autoindex.conf  php74-php.conf  README  userdir.conf  virthost  welcome.conf
[fred@web conf.d]$

[fred@web conf.d]$ sudo systemctl daemon-reload
```

###Configurer la racine web
```bash
[fred@web www]$ sudo chown apache nextcloud -R

[fred@web www]$ ls -al
total 4
drwxr-xr-x.  5 root   root   50 Dec 14 18:23 .
drwxr-xr-x. 22 root   root 4096 Dec 14 16:08 ..
drwxr-xr-x.  2 root   root    6 Nov 15 04:13 cgi-bin
drwxr-xr-x.  2 root   root    6 Nov 15 04:13 html
drwxr-xr-x.  3 apache root   18 Dec 14 18:23 nextcloud

[fred@web nextcloud]$ ls -al
total 0
drwxr-xr-x. 3 apache root 18 Dec 14 18:23 .
drwxr-xr-x. 5 root   root 50 Dec 14 18:23 ..
drwxr-xr-x. 2 apache root  6 Dec 14 18:23 html

[fred@web php74]$ cat /etc/opt/remi/php74/php.ini | grep timezone
; Defines the default timezone used by the date functions
; http://php.net/date.timezone
;date.timezone ="Europe/Paris"
```

###Récupérer Nextcloud
```bash
[fred@web ~]$ unzip nextcloud-21.0.1.zip

[fred@web ~]$ rm nextcloud-21.0.1.zip

[fred@web html]$ sudo chown apache nextcloud -R 
[fred@web html]$ ls -al
total 4
drwxr-xr-x.  3 apache root   23 Dec 14 18:55 .
drwxr-xr-x.  3 apache root   18 Dec 14 18:23 ..
drwxr-xr-x. 13 apache fred 4096 Apr  8  2021 nextcloud

[fred@web html]$ sudo chmod 770 nextcloud -R


[fred@node1 nginx]$ systemctl status nginx
```

###Installer NGINX
```bash
 nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-12-15 15:35:38 CET; 2min 40s ago

[fred@proxy conf.d]$ sudo vi web.tp2.cesi.conf

server {

listen 80;
server_name web.tp2.cesi;

location / {

proxy_pass http:// web.tp2.cesi;
}
}

[fred@proxy conf.d]$ sudo vi /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.2.1.11       web.tp2.cesi
```

###HTTPS Générer une clé et un certificat avec la commande suivante :

[fred@proxy conf.d]$ openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout server.key -out server.crt
Generating a RSA private key
..................................................................................++++
............................................++++
writing new private key to 'server.key'
req: Can't open "server.key" for writing, Permission denied
[fred@proxy conf.d]$ sudo openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout server.key -out server.crt

Generating a RSA private key
..............................................................................................................................................................................................................++++
...................................................................++++
writing new private key to 'server.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:fr
State or Province Name (full name) []:
Locality Name (eg, city) [Default City]:
Organization Name (eg, company) [Default Company Ltd]:CESI
Organizational Unit Name (eg, section) []:
Common Name (eg, your name or your server's hostname) []:
Email Address []:
```
